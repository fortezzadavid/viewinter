# Vragen

Er is een jhipster applicatie (Java met Angular). Start deze applicatie (zie readme), log
in met admin/admin en beantwoord de volgende vragen:

## Vraag 1.

Ga onder het menu "entiteiten" naar Fortezza Employees. Je zie dat er geen resultaten zijn
maar wel 3 entries. Vind de bug zodat de Employees worden weergegeven.

## Vraag 2.

Probeer "Fortezza Employee id=1" te verwijdern. Als je een entry probeert te verwijderen dan krijg je de melding op het scherm
"The HTTP verb you used is not supported for this URL.". Voeg deze methode toe in het endpoint
en zorg ervoor dat dit binnen de FortezzaService correct verwerkt wordt.

## Vraag 3.

Zie vraag 2 maar nu voor het toevoegen van een Fortezza Employee

## Vraag 4.

Zie vraag 2 maar nu voor het wijzigen van een Fortezza Employee

## Vraag 5.

In het scherm met de Fortezza Employees vind de klant het veld "id" niet zoveel toevoegen. Zorg
ervoor dat het niet wordt weergegeven. Zet de leeftijd achteraan.

## Vraag 6.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
