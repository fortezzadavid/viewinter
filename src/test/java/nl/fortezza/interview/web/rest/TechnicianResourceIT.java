package nl.fortezza.interview.web.rest;

import nl.fortezza.interview.FortezzaInterviewApp;
import nl.fortezza.interview.domain.Technician;
import nl.fortezza.interview.repository.TechnicianRepository;
import nl.fortezza.interview.service.TechnicianService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TechnicianResource} REST controller.
 */
@SpringBootTest(classes = FortezzaInterviewApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class TechnicianResourceIT {

    private static final String DEFAULT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private TechnicianRepository technicianRepository;

    @Autowired
    private TechnicianService technicianService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTechnicianMockMvc;

    private Technician technician;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Technician createEntity(EntityManager em) {
        Technician technician = new Technician()
            .number(DEFAULT_NUMBER)
            .name(DEFAULT_NAME);
        return technician;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Technician createUpdatedEntity(EntityManager em) {
        Technician technician = new Technician()
            .number(UPDATED_NUMBER)
            .name(UPDATED_NAME);
        return technician;
    }

    @BeforeEach
    public void initTest() {
        technician = createEntity(em);
    }

    @Test
    @Transactional
    public void createTechnician() throws Exception {
        int databaseSizeBeforeCreate = technicianRepository.findAll().size();
        // Create the Technician
        restTechnicianMockMvc.perform(post("/api/technicians").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(technician)))
            .andExpect(status().isCreated());

        // Validate the Technician in the database
        List<Technician> technicianList = technicianRepository.findAll();
        assertThat(technicianList).hasSize(databaseSizeBeforeCreate + 1);
        Technician testTechnician = technicianList.get(technicianList.size() - 1);
        assertThat(testTechnician.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testTechnician.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createTechnicianWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = technicianRepository.findAll().size();

        // Create the Technician with an existing ID
        technician.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTechnicianMockMvc.perform(post("/api/technicians").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(technician)))
            .andExpect(status().isBadRequest());

        // Validate the Technician in the database
        List<Technician> technicianList = technicianRepository.findAll();
        assertThat(technicianList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = technicianRepository.findAll().size();
        // set the field null
        technician.setNumber(null);

        // Create the Technician, which fails.


        restTechnicianMockMvc.perform(post("/api/technicians").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(technician)))
            .andExpect(status().isBadRequest());

        List<Technician> technicianList = technicianRepository.findAll();
        assertThat(technicianList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = technicianRepository.findAll().size();
        // set the field null
        technician.setName(null);

        // Create the Technician, which fails.


        restTechnicianMockMvc.perform(post("/api/technicians").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(technician)))
            .andExpect(status().isBadRequest());

        List<Technician> technicianList = technicianRepository.findAll();
        assertThat(technicianList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTechnicians() throws Exception {
        // Initialize the database
        technicianRepository.saveAndFlush(technician);

        // Get all the technicianList
        restTechnicianMockMvc.perform(get("/api/technicians?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(technician.getId().intValue())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getTechnician() throws Exception {
        // Initialize the database
        technicianRepository.saveAndFlush(technician);

        // Get the technician
        restTechnicianMockMvc.perform(get("/api/technicians/{id}", technician.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(technician.getId().intValue()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingTechnician() throws Exception {
        // Get the technician
        restTechnicianMockMvc.perform(get("/api/technicians/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTechnician() throws Exception {
        // Initialize the database
        technicianService.save(technician);

        int databaseSizeBeforeUpdate = technicianRepository.findAll().size();

        // Update the technician
        Technician updatedTechnician = technicianRepository.findById(technician.getId()).get();
        // Disconnect from session so that the updates on updatedTechnician are not directly saved in db
        em.detach(updatedTechnician);
        updatedTechnician
            .number(UPDATED_NUMBER)
            .name(UPDATED_NAME);

        restTechnicianMockMvc.perform(put("/api/technicians").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedTechnician)))
            .andExpect(status().isOk());

        // Validate the Technician in the database
        List<Technician> technicianList = technicianRepository.findAll();
        assertThat(technicianList).hasSize(databaseSizeBeforeUpdate);
        Technician testTechnician = technicianList.get(technicianList.size() - 1);
        assertThat(testTechnician.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testTechnician.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingTechnician() throws Exception {
        int databaseSizeBeforeUpdate = technicianRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTechnicianMockMvc.perform(put("/api/technicians").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(technician)))
            .andExpect(status().isBadRequest());

        // Validate the Technician in the database
        List<Technician> technicianList = technicianRepository.findAll();
        assertThat(technicianList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTechnician() throws Exception {
        // Initialize the database
        technicianService.save(technician);

        int databaseSizeBeforeDelete = technicianRepository.findAll().size();

        // Delete the technician
        restTechnicianMockMvc.perform(delete("/api/technicians/{id}", technician.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Technician> technicianList = technicianRepository.findAll();
        assertThat(technicianList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
