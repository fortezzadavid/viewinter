package nl.fortezza.interview.web.rest;

import nl.fortezza.interview.FortezzaInterviewApp;
import nl.fortezza.interview.service.FortezzaService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the FortezzaResource REST controller.
 *
 * @see FortezzaResource
 */
@SpringBootTest(classes = FortezzaInterviewApp.class)
public class FortezzaResourceIT {

    private MockMvc restMockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        FortezzaResource fortezzaResource = new FortezzaResource(new FortezzaService());
        restMockMvc = MockMvcBuilders
            .standaloneSetup(fortezzaResource)
            .build();
    }

    /**
     * Test getFortezza
     */
    @Test
    public void testGetFortezza() throws Exception {
        restMockMvc.perform(get("/api/fortezza/fortezza"))
            .andExpect(status().isOk());
    }
}
