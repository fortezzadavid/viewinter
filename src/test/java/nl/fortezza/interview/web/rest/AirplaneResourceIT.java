package nl.fortezza.interview.web.rest;

import nl.fortezza.interview.FortezzaInterviewApp;
import nl.fortezza.interview.domain.Airplane;
import nl.fortezza.interview.repository.AirplaneRepository;
import nl.fortezza.interview.service.AirplaneService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import nl.fortezza.interview.domain.enumeration.Country;
/**
 * Integration tests for the {@link AirplaneResource} REST controller.
 */
@SpringBootTest(classes = FortezzaInterviewApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AirplaneResourceIT {

    private static final String DEFAULT_REGISTRATION = "AAAAAAAAAA";
    private static final String UPDATED_REGISTRATION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CLUB_OWNED = false;
    private static final Boolean UPDATED_CLUB_OWNED = true;

    private static final Country DEFAULT_COUNTRY = Country.PH;
    private static final Country UPDATED_COUNTRY = Country.OO;

    @Autowired
    private AirplaneRepository airplaneRepository;

    @Autowired
    private AirplaneService airplaneService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAirplaneMockMvc;

    private Airplane airplane;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Airplane createEntity(EntityManager em) {
        Airplane airplane = new Airplane()
            .registration(DEFAULT_REGISTRATION)
            .clubOwned(DEFAULT_CLUB_OWNED)
            .country(DEFAULT_COUNTRY);
        return airplane;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Airplane createUpdatedEntity(EntityManager em) {
        Airplane airplane = new Airplane()
            .registration(UPDATED_REGISTRATION)
            .clubOwned(UPDATED_CLUB_OWNED)
            .country(UPDATED_COUNTRY);
        return airplane;
    }

    @BeforeEach
    public void initTest() {
        airplane = createEntity(em);
    }

    @Test
    @Transactional
    public void createAirplane() throws Exception {
        int databaseSizeBeforeCreate = airplaneRepository.findAll().size();
        // Create the Airplane
        restAirplaneMockMvc.perform(post("/api/airplanes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(airplane)))
            .andExpect(status().isCreated());

        // Validate the Airplane in the database
        List<Airplane> airplaneList = airplaneRepository.findAll();
        assertThat(airplaneList).hasSize(databaseSizeBeforeCreate + 1);
        Airplane testAirplane = airplaneList.get(airplaneList.size() - 1);
        assertThat(testAirplane.getRegistration()).isEqualTo(DEFAULT_REGISTRATION);
        assertThat(testAirplane.isClubOwned()).isEqualTo(DEFAULT_CLUB_OWNED);
        assertThat(testAirplane.getCountry()).isEqualTo(DEFAULT_COUNTRY);
    }

    @Test
    @Transactional
    public void createAirplaneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = airplaneRepository.findAll().size();

        // Create the Airplane with an existing ID
        airplane.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAirplaneMockMvc.perform(post("/api/airplanes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(airplane)))
            .andExpect(status().isBadRequest());

        // Validate the Airplane in the database
        List<Airplane> airplaneList = airplaneRepository.findAll();
        assertThat(airplaneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkRegistrationIsRequired() throws Exception {
        int databaseSizeBeforeTest = airplaneRepository.findAll().size();
        // set the field null
        airplane.setRegistration(null);

        // Create the Airplane, which fails.


        restAirplaneMockMvc.perform(post("/api/airplanes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(airplane)))
            .andExpect(status().isBadRequest());

        List<Airplane> airplaneList = airplaneRepository.findAll();
        assertThat(airplaneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClubOwnedIsRequired() throws Exception {
        int databaseSizeBeforeTest = airplaneRepository.findAll().size();
        // set the field null
        airplane.setClubOwned(null);

        // Create the Airplane, which fails.


        restAirplaneMockMvc.perform(post("/api/airplanes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(airplane)))
            .andExpect(status().isBadRequest());

        List<Airplane> airplaneList = airplaneRepository.findAll();
        assertThat(airplaneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCountryIsRequired() throws Exception {
        int databaseSizeBeforeTest = airplaneRepository.findAll().size();
        // set the field null
        airplane.setCountry(null);

        // Create the Airplane, which fails.


        restAirplaneMockMvc.perform(post("/api/airplanes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(airplane)))
            .andExpect(status().isBadRequest());

        List<Airplane> airplaneList = airplaneRepository.findAll();
        assertThat(airplaneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAirplanes() throws Exception {
        // Initialize the database
        airplaneRepository.saveAndFlush(airplane);

        // Get all the airplaneList
        restAirplaneMockMvc.perform(get("/api/airplanes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(airplane.getId().intValue())))
            .andExpect(jsonPath("$.[*].registration").value(hasItem(DEFAULT_REGISTRATION)))
            .andExpect(jsonPath("$.[*].clubOwned").value(hasItem(DEFAULT_CLUB_OWNED.booleanValue())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())));
    }
    
    @Test
    @Transactional
    public void getAirplane() throws Exception {
        // Initialize the database
        airplaneRepository.saveAndFlush(airplane);

        // Get the airplane
        restAirplaneMockMvc.perform(get("/api/airplanes/{id}", airplane.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(airplane.getId().intValue()))
            .andExpect(jsonPath("$.registration").value(DEFAULT_REGISTRATION))
            .andExpect(jsonPath("$.clubOwned").value(DEFAULT_CLUB_OWNED.booleanValue()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingAirplane() throws Exception {
        // Get the airplane
        restAirplaneMockMvc.perform(get("/api/airplanes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAirplane() throws Exception {
        // Initialize the database
        airplaneService.save(airplane);

        int databaseSizeBeforeUpdate = airplaneRepository.findAll().size();

        // Update the airplane
        Airplane updatedAirplane = airplaneRepository.findById(airplane.getId()).get();
        // Disconnect from session so that the updates on updatedAirplane are not directly saved in db
        em.detach(updatedAirplane);
        updatedAirplane
            .registration(UPDATED_REGISTRATION)
            .clubOwned(UPDATED_CLUB_OWNED)
            .country(UPDATED_COUNTRY);

        restAirplaneMockMvc.perform(put("/api/airplanes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAirplane)))
            .andExpect(status().isOk());

        // Validate the Airplane in the database
        List<Airplane> airplaneList = airplaneRepository.findAll();
        assertThat(airplaneList).hasSize(databaseSizeBeforeUpdate);
        Airplane testAirplane = airplaneList.get(airplaneList.size() - 1);
        assertThat(testAirplane.getRegistration()).isEqualTo(UPDATED_REGISTRATION);
        assertThat(testAirplane.isClubOwned()).isEqualTo(UPDATED_CLUB_OWNED);
        assertThat(testAirplane.getCountry()).isEqualTo(UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void updateNonExistingAirplane() throws Exception {
        int databaseSizeBeforeUpdate = airplaneRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAirplaneMockMvc.perform(put("/api/airplanes").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(airplane)))
            .andExpect(status().isBadRequest());

        // Validate the Airplane in the database
        List<Airplane> airplaneList = airplaneRepository.findAll();
        assertThat(airplaneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAirplane() throws Exception {
        // Initialize the database
        airplaneService.save(airplane);

        int databaseSizeBeforeDelete = airplaneRepository.findAll().size();

        // Delete the airplane
        restAirplaneMockMvc.perform(delete("/api/airplanes/{id}", airplane.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Airplane> airplaneList = airplaneRepository.findAll();
        assertThat(airplaneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
