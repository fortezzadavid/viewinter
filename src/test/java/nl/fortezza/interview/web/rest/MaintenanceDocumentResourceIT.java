package nl.fortezza.interview.web.rest;

import nl.fortezza.interview.FortezzaInterviewApp;
import nl.fortezza.interview.domain.MaintenanceDocument;
import nl.fortezza.interview.repository.MaintenanceDocumentRepository;
import nl.fortezza.interview.service.MaintenanceDocumentService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import nl.fortezza.interview.domain.enumeration.DocumentStatus;
/**
 * Integration tests for the {@link MaintenanceDocumentResource} REST controller.
 */
@SpringBootTest(classes = FortezzaInterviewApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class MaintenanceDocumentResourceIT {

    private static final String DEFAULT_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_FILENAME = "BBBBBBBBBB";

    private static final String DEFAULT_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_VERSION = "BBBBBBBBBB";

    private static final DocumentStatus DEFAULT_STATUS = DocumentStatus.CREATED;
    private static final DocumentStatus UPDATED_STATUS = DocumentStatus.ARCHIVED;

    @Autowired
    private MaintenanceDocumentRepository maintenanceDocumentRepository;

    @Autowired
    private MaintenanceDocumentService maintenanceDocumentService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMaintenanceDocumentMockMvc;

    private MaintenanceDocument maintenanceDocument;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaintenanceDocument createEntity(EntityManager em) {
        MaintenanceDocument maintenanceDocument = new MaintenanceDocument()
            .filename(DEFAULT_FILENAME)
            .version(DEFAULT_VERSION)
            .status(DEFAULT_STATUS);
        return maintenanceDocument;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaintenanceDocument createUpdatedEntity(EntityManager em) {
        MaintenanceDocument maintenanceDocument = new MaintenanceDocument()
            .filename(UPDATED_FILENAME)
            .version(UPDATED_VERSION)
            .status(UPDATED_STATUS);
        return maintenanceDocument;
    }

    @BeforeEach
    public void initTest() {
        maintenanceDocument = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaintenanceDocument() throws Exception {
        int databaseSizeBeforeCreate = maintenanceDocumentRepository.findAll().size();
        // Create the MaintenanceDocument
        restMaintenanceDocumentMockMvc.perform(post("/api/maintenance-documents").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(maintenanceDocument)))
            .andExpect(status().isCreated());

        // Validate the MaintenanceDocument in the database
        List<MaintenanceDocument> maintenanceDocumentList = maintenanceDocumentRepository.findAll();
        assertThat(maintenanceDocumentList).hasSize(databaseSizeBeforeCreate + 1);
        MaintenanceDocument testMaintenanceDocument = maintenanceDocumentList.get(maintenanceDocumentList.size() - 1);
        assertThat(testMaintenanceDocument.getFilename()).isEqualTo(DEFAULT_FILENAME);
        assertThat(testMaintenanceDocument.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testMaintenanceDocument.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createMaintenanceDocumentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = maintenanceDocumentRepository.findAll().size();

        // Create the MaintenanceDocument with an existing ID
        maintenanceDocument.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaintenanceDocumentMockMvc.perform(post("/api/maintenance-documents").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(maintenanceDocument)))
            .andExpect(status().isBadRequest());

        // Validate the MaintenanceDocument in the database
        List<MaintenanceDocument> maintenanceDocumentList = maintenanceDocumentRepository.findAll();
        assertThat(maintenanceDocumentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFilenameIsRequired() throws Exception {
        int databaseSizeBeforeTest = maintenanceDocumentRepository.findAll().size();
        // set the field null
        maintenanceDocument.setFilename(null);

        // Create the MaintenanceDocument, which fails.


        restMaintenanceDocumentMockMvc.perform(post("/api/maintenance-documents").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(maintenanceDocument)))
            .andExpect(status().isBadRequest());

        List<MaintenanceDocument> maintenanceDocumentList = maintenanceDocumentRepository.findAll();
        assertThat(maintenanceDocumentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVersionIsRequired() throws Exception {
        int databaseSizeBeforeTest = maintenanceDocumentRepository.findAll().size();
        // set the field null
        maintenanceDocument.setVersion(null);

        // Create the MaintenanceDocument, which fails.


        restMaintenanceDocumentMockMvc.perform(post("/api/maintenance-documents").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(maintenanceDocument)))
            .andExpect(status().isBadRequest());

        List<MaintenanceDocument> maintenanceDocumentList = maintenanceDocumentRepository.findAll();
        assertThat(maintenanceDocumentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = maintenanceDocumentRepository.findAll().size();
        // set the field null
        maintenanceDocument.setStatus(null);

        // Create the MaintenanceDocument, which fails.


        restMaintenanceDocumentMockMvc.perform(post("/api/maintenance-documents").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(maintenanceDocument)))
            .andExpect(status().isBadRequest());

        List<MaintenanceDocument> maintenanceDocumentList = maintenanceDocumentRepository.findAll();
        assertThat(maintenanceDocumentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMaintenanceDocuments() throws Exception {
        // Initialize the database
        maintenanceDocumentRepository.saveAndFlush(maintenanceDocument);

        // Get all the maintenanceDocumentList
        restMaintenanceDocumentMockMvc.perform(get("/api/maintenance-documents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(maintenanceDocument.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME)))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
    
    @Test
    @Transactional
    public void getMaintenanceDocument() throws Exception {
        // Initialize the database
        maintenanceDocumentRepository.saveAndFlush(maintenanceDocument);

        // Get the maintenanceDocument
        restMaintenanceDocumentMockMvc.perform(get("/api/maintenance-documents/{id}", maintenanceDocument.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(maintenanceDocument.getId().intValue()))
            .andExpect(jsonPath("$.filename").value(DEFAULT_FILENAME))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingMaintenanceDocument() throws Exception {
        // Get the maintenanceDocument
        restMaintenanceDocumentMockMvc.perform(get("/api/maintenance-documents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaintenanceDocument() throws Exception {
        // Initialize the database
        maintenanceDocumentService.save(maintenanceDocument);

        int databaseSizeBeforeUpdate = maintenanceDocumentRepository.findAll().size();

        // Update the maintenanceDocument
        MaintenanceDocument updatedMaintenanceDocument = maintenanceDocumentRepository.findById(maintenanceDocument.getId()).get();
        // Disconnect from session so that the updates on updatedMaintenanceDocument are not directly saved in db
        em.detach(updatedMaintenanceDocument);
        updatedMaintenanceDocument
            .filename(UPDATED_FILENAME)
            .version(UPDATED_VERSION)
            .status(UPDATED_STATUS);

        restMaintenanceDocumentMockMvc.perform(put("/api/maintenance-documents").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaintenanceDocument)))
            .andExpect(status().isOk());

        // Validate the MaintenanceDocument in the database
        List<MaintenanceDocument> maintenanceDocumentList = maintenanceDocumentRepository.findAll();
        assertThat(maintenanceDocumentList).hasSize(databaseSizeBeforeUpdate);
        MaintenanceDocument testMaintenanceDocument = maintenanceDocumentList.get(maintenanceDocumentList.size() - 1);
        assertThat(testMaintenanceDocument.getFilename()).isEqualTo(UPDATED_FILENAME);
        assertThat(testMaintenanceDocument.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testMaintenanceDocument.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingMaintenanceDocument() throws Exception {
        int databaseSizeBeforeUpdate = maintenanceDocumentRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaintenanceDocumentMockMvc.perform(put("/api/maintenance-documents").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(maintenanceDocument)))
            .andExpect(status().isBadRequest());

        // Validate the MaintenanceDocument in the database
        List<MaintenanceDocument> maintenanceDocumentList = maintenanceDocumentRepository.findAll();
        assertThat(maintenanceDocumentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaintenanceDocument() throws Exception {
        // Initialize the database
        maintenanceDocumentService.save(maintenanceDocument);

        int databaseSizeBeforeDelete = maintenanceDocumentRepository.findAll().size();

        // Delete the maintenanceDocument
        restMaintenanceDocumentMockMvc.perform(delete("/api/maintenance-documents/{id}", maintenanceDocument.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MaintenanceDocument> maintenanceDocumentList = maintenanceDocumentRepository.findAll();
        assertThat(maintenanceDocumentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
