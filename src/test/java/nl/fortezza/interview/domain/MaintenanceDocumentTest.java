package nl.fortezza.interview.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import nl.fortezza.interview.web.rest.TestUtil;

public class MaintenanceDocumentTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaintenanceDocument.class);
        MaintenanceDocument maintenanceDocument1 = new MaintenanceDocument();
        maintenanceDocument1.setId(1L);
        MaintenanceDocument maintenanceDocument2 = new MaintenanceDocument();
        maintenanceDocument2.setId(maintenanceDocument1.getId());
        assertThat(maintenanceDocument1).isEqualTo(maintenanceDocument2);
        maintenanceDocument2.setId(2L);
        assertThat(maintenanceDocument1).isNotEqualTo(maintenanceDocument2);
        maintenanceDocument1.setId(null);
        assertThat(maintenanceDocument1).isNotEqualTo(maintenanceDocument2);
    }
}
