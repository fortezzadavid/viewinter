import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TechnicianComponentsPage, TechnicianDeleteDialog, TechnicianUpdatePage } from './technician.page-object';

const expect = chai.expect;

describe('Technician e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let technicianComponentsPage: TechnicianComponentsPage;
  let technicianUpdatePage: TechnicianUpdatePage;
  let technicianDeleteDialog: TechnicianDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Technicians', async () => {
    await navBarPage.goToEntity('technician');
    technicianComponentsPage = new TechnicianComponentsPage();
    await browser.wait(ec.visibilityOf(technicianComponentsPage.title), 5000);
    expect(await technicianComponentsPage.getTitle()).to.eq('fortezzaInterviewApp.technician.home.title');
    await browser.wait(ec.or(ec.visibilityOf(technicianComponentsPage.entities), ec.visibilityOf(technicianComponentsPage.noResult)), 1000);
  });

  it('should load create Technician page', async () => {
    await technicianComponentsPage.clickOnCreateButton();
    technicianUpdatePage = new TechnicianUpdatePage();
    expect(await technicianUpdatePage.getPageTitle()).to.eq('fortezzaInterviewApp.technician.home.createOrEditLabel');
    await technicianUpdatePage.cancel();
  });

  it('should create and save Technicians', async () => {
    const nbButtonsBeforeCreate = await technicianComponentsPage.countDeleteButtons();

    await technicianComponentsPage.clickOnCreateButton();

    await promise.all([technicianUpdatePage.setNumberInput('number'), technicianUpdatePage.setNameInput('name')]);

    expect(await technicianUpdatePage.getNumberInput()).to.eq('number', 'Expected Number value to be equals to number');
    expect(await technicianUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');

    await technicianUpdatePage.save();
    expect(await technicianUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await technicianComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Technician', async () => {
    const nbButtonsBeforeDelete = await technicianComponentsPage.countDeleteButtons();
    await technicianComponentsPage.clickOnLastDeleteButton();

    technicianDeleteDialog = new TechnicianDeleteDialog();
    expect(await technicianDeleteDialog.getDialogTitle()).to.eq('fortezzaInterviewApp.technician.delete.question');
    await technicianDeleteDialog.clickOnConfirmButton();

    expect(await technicianComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
