import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AirplaneComponentsPage, AirplaneDeleteDialog, AirplaneUpdatePage } from './airplane.page-object';

const expect = chai.expect;

describe('Airplane e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let airplaneComponentsPage: AirplaneComponentsPage;
  let airplaneUpdatePage: AirplaneUpdatePage;
  let airplaneDeleteDialog: AirplaneDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Airplanes', async () => {
    await navBarPage.goToEntity('airplane');
    airplaneComponentsPage = new AirplaneComponentsPage();
    await browser.wait(ec.visibilityOf(airplaneComponentsPage.title), 5000);
    expect(await airplaneComponentsPage.getTitle()).to.eq('fortezzaInterviewApp.airplane.home.title');
    await browser.wait(ec.or(ec.visibilityOf(airplaneComponentsPage.entities), ec.visibilityOf(airplaneComponentsPage.noResult)), 1000);
  });

  it('should load create Airplane page', async () => {
    await airplaneComponentsPage.clickOnCreateButton();
    airplaneUpdatePage = new AirplaneUpdatePage();
    expect(await airplaneUpdatePage.getPageTitle()).to.eq('fortezzaInterviewApp.airplane.home.createOrEditLabel');
    await airplaneUpdatePage.cancel();
  });

  it('should create and save Airplanes', async () => {
    const nbButtonsBeforeCreate = await airplaneComponentsPage.countDeleteButtons();

    await airplaneComponentsPage.clickOnCreateButton();

    await promise.all([airplaneUpdatePage.setRegistrationInput('registration'), airplaneUpdatePage.countrySelectLastOption()]);

    expect(await airplaneUpdatePage.getRegistrationInput()).to.eq(
      'registration',
      'Expected Registration value to be equals to registration'
    );
    const selectedClubOwned = airplaneUpdatePage.getClubOwnedInput();
    if (await selectedClubOwned.isSelected()) {
      await airplaneUpdatePage.getClubOwnedInput().click();
      expect(await airplaneUpdatePage.getClubOwnedInput().isSelected(), 'Expected clubOwned not to be selected').to.be.false;
    } else {
      await airplaneUpdatePage.getClubOwnedInput().click();
      expect(await airplaneUpdatePage.getClubOwnedInput().isSelected(), 'Expected clubOwned to be selected').to.be.true;
    }

    await airplaneUpdatePage.save();
    expect(await airplaneUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await airplaneComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Airplane', async () => {
    const nbButtonsBeforeDelete = await airplaneComponentsPage.countDeleteButtons();
    await airplaneComponentsPage.clickOnLastDeleteButton();

    airplaneDeleteDialog = new AirplaneDeleteDialog();
    expect(await airplaneDeleteDialog.getDialogTitle()).to.eq('fortezzaInterviewApp.airplane.delete.question');
    await airplaneDeleteDialog.clickOnConfirmButton();

    expect(await airplaneComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
