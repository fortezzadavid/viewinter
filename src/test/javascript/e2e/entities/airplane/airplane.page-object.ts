import { element, by, ElementFinder } from 'protractor';

export class AirplaneComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-airplane div table .btn-danger'));
  title = element.all(by.css('jhi-airplane div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class AirplaneUpdatePage {
  pageTitle = element(by.id('jhi-airplane-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  registrationInput = element(by.id('field_registration'));
  clubOwnedInput = element(by.id('field_clubOwned'));
  countrySelect = element(by.id('field_country'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setRegistrationInput(registration: string): Promise<void> {
    await this.registrationInput.sendKeys(registration);
  }

  async getRegistrationInput(): Promise<string> {
    return await this.registrationInput.getAttribute('value');
  }

  getClubOwnedInput(): ElementFinder {
    return this.clubOwnedInput;
  }

  async setCountrySelect(country: string): Promise<void> {
    await this.countrySelect.sendKeys(country);
  }

  async getCountrySelect(): Promise<string> {
    return await this.countrySelect.element(by.css('option:checked')).getText();
  }

  async countrySelectLastOption(): Promise<void> {
    await this.countrySelect.all(by.tagName('option')).last().click();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class AirplaneDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-airplane-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-airplane'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
