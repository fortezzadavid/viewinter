import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  MaintenanceDocumentComponentsPage,
  MaintenanceDocumentDeleteDialog,
  MaintenanceDocumentUpdatePage,
} from './maintenance-document.page-object';

const expect = chai.expect;

describe('MaintenanceDocument e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let maintenanceDocumentComponentsPage: MaintenanceDocumentComponentsPage;
  let maintenanceDocumentUpdatePage: MaintenanceDocumentUpdatePage;
  let maintenanceDocumentDeleteDialog: MaintenanceDocumentDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load MaintenanceDocuments', async () => {
    await navBarPage.goToEntity('maintenance-document');
    maintenanceDocumentComponentsPage = new MaintenanceDocumentComponentsPage();
    await browser.wait(ec.visibilityOf(maintenanceDocumentComponentsPage.title), 5000);
    expect(await maintenanceDocumentComponentsPage.getTitle()).to.eq('fortezzaInterviewApp.maintenanceDocument.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(maintenanceDocumentComponentsPage.entities), ec.visibilityOf(maintenanceDocumentComponentsPage.noResult)),
      1000
    );
  });

  it('should load create MaintenanceDocument page', async () => {
    await maintenanceDocumentComponentsPage.clickOnCreateButton();
    maintenanceDocumentUpdatePage = new MaintenanceDocumentUpdatePage();
    expect(await maintenanceDocumentUpdatePage.getPageTitle()).to.eq('fortezzaInterviewApp.maintenanceDocument.home.createOrEditLabel');
    await maintenanceDocumentUpdatePage.cancel();
  });

  it('should create and save MaintenanceDocuments', async () => {
    const nbButtonsBeforeCreate = await maintenanceDocumentComponentsPage.countDeleteButtons();

    await maintenanceDocumentComponentsPage.clickOnCreateButton();

    await promise.all([
      maintenanceDocumentUpdatePage.setFilenameInput('filename'),
      maintenanceDocumentUpdatePage.setVersionInput('version'),
      maintenanceDocumentUpdatePage.statusSelectLastOption(),
      maintenanceDocumentUpdatePage.airplaneSelectLastOption(),
      maintenanceDocumentUpdatePage.typeSelectLastOption(),
      maintenanceDocumentUpdatePage.technicianSelectLastOption(),
    ]);

    expect(await maintenanceDocumentUpdatePage.getFilenameInput()).to.eq('filename', 'Expected Filename value to be equals to filename');
    expect(await maintenanceDocumentUpdatePage.getVersionInput()).to.eq('version', 'Expected Version value to be equals to version');

    await maintenanceDocumentUpdatePage.save();
    expect(await maintenanceDocumentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await maintenanceDocumentComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last MaintenanceDocument', async () => {
    const nbButtonsBeforeDelete = await maintenanceDocumentComponentsPage.countDeleteButtons();
    await maintenanceDocumentComponentsPage.clickOnLastDeleteButton();

    maintenanceDocumentDeleteDialog = new MaintenanceDocumentDeleteDialog();
    expect(await maintenanceDocumentDeleteDialog.getDialogTitle()).to.eq('fortezzaInterviewApp.maintenanceDocument.delete.question');
    await maintenanceDocumentDeleteDialog.clickOnConfirmButton();

    expect(await maintenanceDocumentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
