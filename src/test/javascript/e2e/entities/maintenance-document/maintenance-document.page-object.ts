import { element, by, ElementFinder } from 'protractor';

export class MaintenanceDocumentComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-maintenance-document div table .btn-danger'));
  title = element.all(by.css('jhi-maintenance-document div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class MaintenanceDocumentUpdatePage {
  pageTitle = element(by.id('jhi-maintenance-document-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  filenameInput = element(by.id('field_filename'));
  versionInput = element(by.id('field_version'));
  statusSelect = element(by.id('field_status'));

  airplaneSelect = element(by.id('field_airplane'));
  typeSelect = element(by.id('field_type'));
  technicianSelect = element(by.id('field_technician'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setFilenameInput(filename: string): Promise<void> {
    await this.filenameInput.sendKeys(filename);
  }

  async getFilenameInput(): Promise<string> {
    return await this.filenameInput.getAttribute('value');
  }

  async setVersionInput(version: string): Promise<void> {
    await this.versionInput.sendKeys(version);
  }

  async getVersionInput(): Promise<string> {
    return await this.versionInput.getAttribute('value');
  }

  async setStatusSelect(status: string): Promise<void> {
    await this.statusSelect.sendKeys(status);
  }

  async getStatusSelect(): Promise<string> {
    return await this.statusSelect.element(by.css('option:checked')).getText();
  }

  async statusSelectLastOption(): Promise<void> {
    await this.statusSelect.all(by.tagName('option')).last().click();
  }

  async airplaneSelectLastOption(): Promise<void> {
    await this.airplaneSelect.all(by.tagName('option')).last().click();
  }

  async airplaneSelectOption(option: string): Promise<void> {
    await this.airplaneSelect.sendKeys(option);
  }

  getAirplaneSelect(): ElementFinder {
    return this.airplaneSelect;
  }

  async getAirplaneSelectedOption(): Promise<string> {
    return await this.airplaneSelect.element(by.css('option:checked')).getText();
  }

  async typeSelectLastOption(): Promise<void> {
    await this.typeSelect.all(by.tagName('option')).last().click();
  }

  async typeSelectOption(option: string): Promise<void> {
    await this.typeSelect.sendKeys(option);
  }

  getTypeSelect(): ElementFinder {
    return this.typeSelect;
  }

  async getTypeSelectedOption(): Promise<string> {
    return await this.typeSelect.element(by.css('option:checked')).getText();
  }

  async technicianSelectLastOption(): Promise<void> {
    await this.technicianSelect.all(by.tagName('option')).last().click();
  }

  async technicianSelectOption(option: string): Promise<void> {
    await this.technicianSelect.sendKeys(option);
  }

  getTechnicianSelect(): ElementFinder {
    return this.technicianSelect;
  }

  async getTechnicianSelectedOption(): Promise<string> {
    return await this.technicianSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class MaintenanceDocumentDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-maintenanceDocument-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-maintenanceDocument'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
