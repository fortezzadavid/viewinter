import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  FortezzaEmployeesComponentsPage,
  FortezzaEmployeesDeleteDialog,
  FortezzaEmployeesUpdatePage,
} from './fortezza-employees.page-object';

const expect = chai.expect;

describe('FortezzaEmployees e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fortezzaEmployeesComponentsPage: FortezzaEmployeesComponentsPage;
  let fortezzaEmployeesUpdatePage: FortezzaEmployeesUpdatePage;
  let fortezzaEmployeesDeleteDialog: FortezzaEmployeesDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FortezzaEmployees', async () => {
    await navBarPage.goToEntity('fortezza-employees');
    fortezzaEmployeesComponentsPage = new FortezzaEmployeesComponentsPage();
    await browser.wait(ec.visibilityOf(fortezzaEmployeesComponentsPage.title), 5000);
    expect(await fortezzaEmployeesComponentsPage.getTitle()).to.eq('fortezzaInterviewApp.fortezzaEmployees.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(fortezzaEmployeesComponentsPage.entities), ec.visibilityOf(fortezzaEmployeesComponentsPage.noResult)),
      1000
    );
  });

  it('should load create FortezzaEmployees page', async () => {
    await fortezzaEmployeesComponentsPage.clickOnCreateButton();
    fortezzaEmployeesUpdatePage = new FortezzaEmployeesUpdatePage();
    expect(await fortezzaEmployeesUpdatePage.getPageTitle()).to.eq('fortezzaInterviewApp.fortezzaEmployees.home.createOrEditLabel');
    await fortezzaEmployeesUpdatePage.cancel();
  });

  it('should create and save FortezzaEmployees', async () => {
    const nbButtonsBeforeCreate = await fortezzaEmployeesComponentsPage.countDeleteButtons();

    await fortezzaEmployeesComponentsPage.clickOnCreateButton();

    await promise.all([
      fortezzaEmployeesUpdatePage.setNameInput('name'),
      fortezzaEmployeesUpdatePage.setAgeInput('5'),
      fortezzaEmployeesUpdatePage.setCityInput('city'),
    ]);

    expect(await fortezzaEmployeesUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await fortezzaEmployeesUpdatePage.getAgeInput()).to.eq('5', 'Expected age value to be equals to 5');
    expect(await fortezzaEmployeesUpdatePage.getCityInput()).to.eq('city', 'Expected City value to be equals to city');

    await fortezzaEmployeesUpdatePage.save();
    expect(await fortezzaEmployeesUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await fortezzaEmployeesComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last FortezzaEmployees', async () => {
    const nbButtonsBeforeDelete = await fortezzaEmployeesComponentsPage.countDeleteButtons();
    await fortezzaEmployeesComponentsPage.clickOnLastDeleteButton();

    fortezzaEmployeesDeleteDialog = new FortezzaEmployeesDeleteDialog();
    expect(await fortezzaEmployeesDeleteDialog.getDialogTitle()).to.eq('fortezzaInterviewApp.fortezzaEmployees.delete.question');
    await fortezzaEmployeesDeleteDialog.clickOnConfirmButton();

    expect(await fortezzaEmployeesComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
