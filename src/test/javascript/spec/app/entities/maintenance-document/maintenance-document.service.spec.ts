import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MaintenanceDocumentService } from 'app/entities/maintenance-document/maintenance-document.service';
import { IMaintenanceDocument, MaintenanceDocument } from 'app/shared/model/maintenance-document.model';
import { DocumentStatus } from 'app/shared/model/enumerations/document-status.model';

describe('Service Tests', () => {
  describe('MaintenanceDocument Service', () => {
    let injector: TestBed;
    let service: MaintenanceDocumentService;
    let httpMock: HttpTestingController;
    let elemDefault: IMaintenanceDocument;
    let expectedResult: IMaintenanceDocument | IMaintenanceDocument[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(MaintenanceDocumentService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new MaintenanceDocument(0, 'AAAAAAA', 'AAAAAAA', DocumentStatus.CREATED);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a MaintenanceDocument', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new MaintenanceDocument()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a MaintenanceDocument', () => {
        const returnedFromService = Object.assign(
          {
            filename: 'BBBBBB',
            version: 'BBBBBB',
            status: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of MaintenanceDocument', () => {
        const returnedFromService = Object.assign(
          {
            filename: 'BBBBBB',
            version: 'BBBBBB',
            status: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a MaintenanceDocument', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
