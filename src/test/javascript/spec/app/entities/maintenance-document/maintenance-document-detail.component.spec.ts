import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FortezzaInterviewTestModule } from '../../../test.module';
import { MaintenanceDocumentDetailComponent } from 'app/entities/maintenance-document/maintenance-document-detail.component';
import { MaintenanceDocument } from 'app/shared/model/maintenance-document.model';

describe('Component Tests', () => {
  describe('MaintenanceDocument Management Detail Component', () => {
    let comp: MaintenanceDocumentDetailComponent;
    let fixture: ComponentFixture<MaintenanceDocumentDetailComponent>;
    const route = ({ data: of({ maintenanceDocument: new MaintenanceDocument(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FortezzaInterviewTestModule],
        declarations: [MaintenanceDocumentDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(MaintenanceDocumentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MaintenanceDocumentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load maintenanceDocument on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.maintenanceDocument).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
