import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FortezzaInterviewTestModule } from '../../../test.module';
import { MaintenanceDocumentUpdateComponent } from 'app/entities/maintenance-document/maintenance-document-update.component';
import { MaintenanceDocumentService } from 'app/entities/maintenance-document/maintenance-document.service';
import { MaintenanceDocument } from 'app/shared/model/maintenance-document.model';

describe('Component Tests', () => {
  describe('MaintenanceDocument Management Update Component', () => {
    let comp: MaintenanceDocumentUpdateComponent;
    let fixture: ComponentFixture<MaintenanceDocumentUpdateComponent>;
    let service: MaintenanceDocumentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FortezzaInterviewTestModule],
        declarations: [MaintenanceDocumentUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(MaintenanceDocumentUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MaintenanceDocumentUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MaintenanceDocumentService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaintenanceDocument(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MaintenanceDocument();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
