import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FortezzaInterviewTestModule } from '../../../test.module';
import { TechnicianDetailComponent } from 'app/entities/technician/technician-detail.component';
import { Technician } from 'app/shared/model/technician.model';

describe('Component Tests', () => {
  describe('Technician Management Detail Component', () => {
    let comp: TechnicianDetailComponent;
    let fixture: ComponentFixture<TechnicianDetailComponent>;
    const route = ({ data: of({ technician: new Technician(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FortezzaInterviewTestModule],
        declarations: [TechnicianDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TechnicianDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TechnicianDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load technician on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.technician).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
