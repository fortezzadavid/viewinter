import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FortezzaInterviewTestModule } from '../../../test.module';
import { TechnicianUpdateComponent } from 'app/entities/technician/technician-update.component';
import { TechnicianService } from 'app/entities/technician/technician.service';
import { Technician } from 'app/shared/model/technician.model';

describe('Component Tests', () => {
  describe('Technician Management Update Component', () => {
    let comp: TechnicianUpdateComponent;
    let fixture: ComponentFixture<TechnicianUpdateComponent>;
    let service: TechnicianService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FortezzaInterviewTestModule],
        declarations: [TechnicianUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(TechnicianUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TechnicianUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TechnicianService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Technician(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Technician();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
