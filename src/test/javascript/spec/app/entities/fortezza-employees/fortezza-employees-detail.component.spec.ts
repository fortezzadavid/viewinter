import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FortezzaInterviewTestModule } from '../../../test.module';
import { FortezzaEmployeesDetailComponent } from 'app/entities/fortezza-employees/fortezza-employees-detail.component';
import { FortezzaEmployees } from 'app/shared/model/fortezza-employees.model';

describe('Component Tests', () => {
  describe('FortezzaEmployees Management Detail Component', () => {
    let comp: FortezzaEmployeesDetailComponent;
    let fixture: ComponentFixture<FortezzaEmployeesDetailComponent>;
    const route = ({ data: of({ fortezzaEmployees: new FortezzaEmployees(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FortezzaInterviewTestModule],
        declarations: [FortezzaEmployeesDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FortezzaEmployeesDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FortezzaEmployeesDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fortezzaEmployees on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fortezzaEmployees).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
