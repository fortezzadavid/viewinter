import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FortezzaInterviewTestModule } from '../../../test.module';
import { FortezzaEmployeesUpdateComponent } from 'app/entities/fortezza-employees/fortezza-employees-update.component';
import { FortezzaEmployeesService } from 'app/entities/fortezza-employees/fortezza-employees.service';
import { FortezzaEmployees } from 'app/shared/model/fortezza-employees.model';

describe('Component Tests', () => {
  describe('FortezzaEmployees Management Update Component', () => {
    let comp: FortezzaEmployeesUpdateComponent;
    let fixture: ComponentFixture<FortezzaEmployeesUpdateComponent>;
    let service: FortezzaEmployeesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FortezzaInterviewTestModule],
        declarations: [FortezzaEmployeesUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FortezzaEmployeesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FortezzaEmployeesUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FortezzaEmployeesService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FortezzaEmployees(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FortezzaEmployees();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
