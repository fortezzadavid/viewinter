import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FortezzaInterviewTestModule } from '../../../test.module';
import { FortezzaEmployeesComponent } from 'app/entities/fortezza-employees/fortezza-employees.component';
import { FortezzaEmployeesService } from 'app/entities/fortezza-employees/fortezza-employees.service';
import { FortezzaEmployees } from 'app/shared/model/fortezza-employees.model';

describe('Component Tests', () => {
  describe('FortezzaEmployees Management Component', () => {
    let comp: FortezzaEmployeesComponent;
    let fixture: ComponentFixture<FortezzaEmployeesComponent>;
    let service: FortezzaEmployeesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FortezzaInterviewTestModule],
        declarations: [FortezzaEmployeesComponent],
      })
        .overrideTemplate(FortezzaEmployeesComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FortezzaEmployeesComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FortezzaEmployeesService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FortezzaEmployees(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fortezzaEmployees && comp.fortezzaEmployees[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
