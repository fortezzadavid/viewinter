import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { FortezzaInterviewTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { FortezzaEmployeesDeleteDialogComponent } from 'app/entities/fortezza-employees/fortezza-employees-delete-dialog.component';
import { FortezzaEmployeesService } from 'app/entities/fortezza-employees/fortezza-employees.service';

describe('Component Tests', () => {
  describe('FortezzaEmployees Management Delete Component', () => {
    let comp: FortezzaEmployeesDeleteDialogComponent;
    let fixture: ComponentFixture<FortezzaEmployeesDeleteDialogComponent>;
    let service: FortezzaEmployeesService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FortezzaInterviewTestModule],
        declarations: [FortezzaEmployeesDeleteDialogComponent],
      })
        .overrideTemplate(FortezzaEmployeesDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FortezzaEmployeesDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FortezzaEmployeesService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
