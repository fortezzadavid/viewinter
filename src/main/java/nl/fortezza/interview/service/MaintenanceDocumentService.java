package nl.fortezza.interview.service;

import nl.fortezza.interview.domain.MaintenanceDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MaintenanceDocument}.
 */
public interface MaintenanceDocumentService {

    /**
     * Save a maintenanceDocument.
     *
     * @param maintenanceDocument the entity to save.
     * @return the persisted entity.
     */
    MaintenanceDocument save(MaintenanceDocument maintenanceDocument);

    /**
     * Get all the maintenanceDocuments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaintenanceDocument> findAll(Pageable pageable);


    /**
     * Get the "id" maintenanceDocument.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaintenanceDocument> findOne(Long id);

    /**
     * Delete the "id" maintenanceDocument.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
