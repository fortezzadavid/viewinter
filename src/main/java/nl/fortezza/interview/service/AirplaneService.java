package nl.fortezza.interview.service;

import nl.fortezza.interview.domain.Airplane;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Airplane}.
 */
public interface AirplaneService {

    /**
     * Save a airplane.
     *
     * @param airplane the entity to save.
     * @return the persisted entity.
     */
    Airplane save(Airplane airplane);

    /**
     * Get all the airplanes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Airplane> findAll(Pageable pageable);


    /**
     * Get the "id" airplane.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Airplane> findOne(Long id);

    /**
     * Delete the "id" airplane.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
