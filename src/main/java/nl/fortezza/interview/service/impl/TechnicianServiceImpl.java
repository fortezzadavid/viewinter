package nl.fortezza.interview.service.impl;

import nl.fortezza.interview.service.TechnicianService;
import nl.fortezza.interview.domain.Technician;
import nl.fortezza.interview.repository.TechnicianRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Technician}.
 */
@Service
@Transactional
public class TechnicianServiceImpl implements TechnicianService {

    private final Logger log = LoggerFactory.getLogger(TechnicianServiceImpl.class);

    private final TechnicianRepository technicianRepository;

    public TechnicianServiceImpl(TechnicianRepository technicianRepository) {
        this.technicianRepository = technicianRepository;
    }

    @Override
    public Technician save(Technician technician) {
        log.debug("Request to save Technician : {}", technician);
        return technicianRepository.save(technician);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Technician> findAll(Pageable pageable) {
        log.debug("Request to get all Technicians");
        return technicianRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Technician> findOne(Long id) {
        log.debug("Request to get Technician : {}", id);
        return technicianRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Technician : {}", id);
        technicianRepository.deleteById(id);
    }
}
