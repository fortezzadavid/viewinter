package nl.fortezza.interview.service.impl;

import nl.fortezza.interview.service.AirplaneService;
import nl.fortezza.interview.domain.Airplane;
import nl.fortezza.interview.repository.AirplaneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Airplane}.
 */
@Service
@Transactional
public class AirplaneServiceImpl implements AirplaneService {

    private final Logger log = LoggerFactory.getLogger(AirplaneServiceImpl.class);

    private final AirplaneRepository airplaneRepository;

    public AirplaneServiceImpl(AirplaneRepository airplaneRepository) {
        this.airplaneRepository = airplaneRepository;
    }

    @Override
    public Airplane save(Airplane airplane) {
        log.debug("Request to save Airplane : {}", airplane);
        return airplaneRepository.save(airplane);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Airplane> findAll(Pageable pageable) {
        log.debug("Request to get all Airplanes");
        return airplaneRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Airplane> findOne(Long id) {
        log.debug("Request to get Airplane : {}", id);
        return airplaneRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Airplane : {}", id);
        airplaneRepository.deleteById(id);
    }
}
