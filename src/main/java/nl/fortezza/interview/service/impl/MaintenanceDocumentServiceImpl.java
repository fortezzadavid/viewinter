package nl.fortezza.interview.service.impl;

import nl.fortezza.interview.service.MaintenanceDocumentService;
import nl.fortezza.interview.domain.MaintenanceDocument;
import nl.fortezza.interview.repository.MaintenanceDocumentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MaintenanceDocument}.
 */
@Service
@Transactional
public class MaintenanceDocumentServiceImpl implements MaintenanceDocumentService {

    private final Logger log = LoggerFactory.getLogger(MaintenanceDocumentServiceImpl.class);

    private final MaintenanceDocumentRepository maintenanceDocumentRepository;

    public MaintenanceDocumentServiceImpl(MaintenanceDocumentRepository maintenanceDocumentRepository) {
        this.maintenanceDocumentRepository = maintenanceDocumentRepository;
    }

    @Override
    public MaintenanceDocument save(MaintenanceDocument maintenanceDocument) {
        log.debug("Request to save MaintenanceDocument : {}", maintenanceDocument);
        return maintenanceDocumentRepository.save(maintenanceDocument);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MaintenanceDocument> findAll(Pageable pageable) {
        log.debug("Request to get all MaintenanceDocuments");
        return maintenanceDocumentRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<MaintenanceDocument> findOne(Long id) {
        log.debug("Request to get MaintenanceDocument : {}", id);
        return maintenanceDocumentRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaintenanceDocument : {}", id);
        maintenanceDocumentRepository.deleteById(id);
    }
}
