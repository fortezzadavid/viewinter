package nl.fortezza.interview.service;

import nl.fortezza.interview.domain.Technician;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Technician}.
 */
public interface TechnicianService {

    /**
     * Save a technician.
     *
     * @param technician the entity to save.
     * @return the persisted entity.
     */
    Technician save(Technician technician);

    /**
     * Get all the technicians.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Technician> findAll(Pageable pageable);


    /**
     * Get the "id" technician.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Technician> findOne(Long id);

    /**
     * Delete the "id" technician.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
