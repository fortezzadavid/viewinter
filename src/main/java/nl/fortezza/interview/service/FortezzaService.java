package nl.fortezza.interview.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.fortezza.interview.domain.FortezzaEmployee;

@Service
@Transactional
public class FortezzaService {

    private final Logger log = LoggerFactory.getLogger(FortezzaService.class);

    public String getEmployees() {
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = null;
        try {
            jsonString = mapper.writeValueAsString(createEmployee1());
        } catch (JsonProcessingException e) {
            jsonString = "";
        }
        return jsonString;
    }

    // todo fix
    private FortezzaEmployee[] createEmployee1() {
        return new FortezzaEmployee[]{
            new FortezzaEmployee(1L, "jan", 42, "Delft"),
            new FortezzaEmployee(2L, "piet", 22, "Rotterdam"),
            new FortezzaEmployee(3L, "irma", 62, "Zoetermeer")
        };
    }
}

//        return "[\n" +
//            "  {\n" +
//            "    \"id\": 1,\n" +
//            "    \"name\": \"jan\",\n" +
//            "    \"age\": 42,\n" +
//            "    \"city\": \"Delft\"\n" +
//            "  },\n" +
//            "  {\n" +
//            "    \"id\": 2,\n" +
//            "    \"name\": \"piet\",\n" +
//            "    \"age\": 22,\n" +
//            "    \"city\": \"Rotterdam\"\n" +
//            "  },\n" +
//            "  {\n" +
//            "    \"id\": 3,\n" +
//            "    \"name\": \"irma\",\n" +
//            "    \"age\": 78,\n" +
//            "    \"city\": \"Zoetermeer\"\n" +
//            "  }\n" +
//            "]";
