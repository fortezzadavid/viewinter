package nl.fortezza.interview.repository;

import nl.fortezza.interview.domain.Technician;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Technician entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TechnicianRepository extends JpaRepository<Technician, Long> {
}
