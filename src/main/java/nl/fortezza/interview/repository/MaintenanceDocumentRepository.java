package nl.fortezza.interview.repository;

import nl.fortezza.interview.domain.MaintenanceDocument;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the MaintenanceDocument entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaintenanceDocumentRepository extends JpaRepository<MaintenanceDocument, Long> {
}
