package nl.fortezza.interview.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import nl.fortezza.interview.domain.enumeration.Country;

/**
 * A Airplane.
 */
@Entity
@Table(name = "airplane")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Airplane implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "registration", nullable = false)
    private String registration;

    @NotNull
    @Column(name = "club_owned", nullable = false)
    private Boolean clubOwned;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "country", nullable = false)
    private Country country;

    @OneToMany(mappedBy = "airplane")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<MaintenanceDocument> maintenanceDocuments = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegistration() {
        return registration;
    }

    public Airplane registration(String registration) {
        this.registration = registration;
        return this;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public Boolean isClubOwned() {
        return clubOwned;
    }

    public Airplane clubOwned(Boolean clubOwned) {
        this.clubOwned = clubOwned;
        return this;
    }

    public void setClubOwned(Boolean clubOwned) {
        this.clubOwned = clubOwned;
    }

    public Country getCountry() {
        return country;
    }

    public Airplane country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Set<MaintenanceDocument> getMaintenanceDocuments() {
        return maintenanceDocuments;
    }

    public Airplane maintenanceDocuments(Set<MaintenanceDocument> maintenanceDocuments) {
        this.maintenanceDocuments = maintenanceDocuments;
        return this;
    }

    public Airplane addMaintenanceDocument(MaintenanceDocument maintenanceDocument) {
        this.maintenanceDocuments.add(maintenanceDocument);
        maintenanceDocument.setAirplane(this);
        return this;
    }

    public Airplane removeMaintenanceDocument(MaintenanceDocument maintenanceDocument) {
        this.maintenanceDocuments.remove(maintenanceDocument);
        maintenanceDocument.setAirplane(null);
        return this;
    }

    public void setMaintenanceDocuments(Set<MaintenanceDocument> maintenanceDocuments) {
        this.maintenanceDocuments = maintenanceDocuments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Airplane)) {
            return false;
        }
        return id != null && id.equals(((Airplane) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Airplane{" +
            "id=" + getId() +
            ", registration='" + getRegistration() + "'" +
            ", clubOwned='" + isClubOwned() + "'" +
            ", country='" + getCountry() + "'" +
            "}";
    }
}
