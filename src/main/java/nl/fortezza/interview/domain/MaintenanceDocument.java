package nl.fortezza.interview.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import nl.fortezza.interview.domain.enumeration.DocumentStatus;

/**
 * A MaintenanceDocument.
 */
@Entity
@Table(name = "maintenance_document")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MaintenanceDocument implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "filename", nullable = false)
    private String filename;

    @NotNull
    @Column(name = "version", nullable = false)
    private String version;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private DocumentStatus status;

    @ManyToOne
    @JsonIgnoreProperties(value = "maintenanceDocuments", allowSetters = true)
    private Airplane airplane;

    @ManyToOne
    @JsonIgnoreProperties(value = "maintenanceDocuments", allowSetters = true)
    private DocumentType type;

    @ManyToOne
    @JsonIgnoreProperties(value = "maintenanceDocuments", allowSetters = true)
    private Technician technician;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public MaintenanceDocument filename(String filename) {
        this.filename = filename;
        return this;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getVersion() {
        return version;
    }

    public MaintenanceDocument version(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public DocumentStatus getStatus() {
        return status;
    }

    public MaintenanceDocument status(DocumentStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(DocumentStatus status) {
        this.status = status;
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public MaintenanceDocument airplane(Airplane airplane) {
        this.airplane = airplane;
        return this;
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

    public DocumentType getType() {
        return type;
    }

    public MaintenanceDocument type(DocumentType documentType) {
        this.type = documentType;
        return this;
    }

    public void setType(DocumentType documentType) {
        this.type = documentType;
    }

    public Technician getTechnician() {
        return technician;
    }

    public MaintenanceDocument technician(Technician technician) {
        this.technician = technician;
        return this;
    }

    public void setTechnician(Technician technician) {
        this.technician = technician;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaintenanceDocument)) {
            return false;
        }
        return id != null && id.equals(((MaintenanceDocument) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MaintenanceDocument{" +
            "id=" + getId() +
            ", filename='" + getFilename() + "'" +
            ", version='" + getVersion() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
