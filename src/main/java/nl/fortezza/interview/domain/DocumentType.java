package nl.fortezza.interview.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DocumentType.
 */
@Entity
@Table(name = "document_type")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DocumentType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "desciption", nullable = false)
    private String desciption;

    @OneToMany(mappedBy = "type")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<MaintenanceDocument> maintenanceDocuments = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public DocumentType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesciption() {
        return desciption;
    }

    public DocumentType desciption(String desciption) {
        this.desciption = desciption;
        return this;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public Set<MaintenanceDocument> getMaintenanceDocuments() {
        return maintenanceDocuments;
    }

    public DocumentType maintenanceDocuments(Set<MaintenanceDocument> maintenanceDocuments) {
        this.maintenanceDocuments = maintenanceDocuments;
        return this;
    }

    public DocumentType addMaintenanceDocument(MaintenanceDocument maintenanceDocument) {
        this.maintenanceDocuments.add(maintenanceDocument);
        maintenanceDocument.setType(this);
        return this;
    }

    public DocumentType removeMaintenanceDocument(MaintenanceDocument maintenanceDocument) {
        this.maintenanceDocuments.remove(maintenanceDocument);
        maintenanceDocument.setType(null);
        return this;
    }

    public void setMaintenanceDocuments(Set<MaintenanceDocument> maintenanceDocuments) {
        this.maintenanceDocuments = maintenanceDocuments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DocumentType)) {
            return false;
        }
        return id != null && id.equals(((DocumentType) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DocumentType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", desciption='" + getDesciption() + "'" +
            "}";
    }
}
