package nl.fortezza.interview.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class FortezzaEmployee {
    private Long id;
    private String name;
    private Integer age;
    private String city;

    public FortezzaEmployee(Long id, String name, Integer age, String city) {
        this.age = age;
        this.id = id;
        this.city = city;
        this.name = name;
    }
}
