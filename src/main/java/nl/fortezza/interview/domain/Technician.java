package nl.fortezza.interview.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Technician.
 */
@Entity
@Table(name = "technician")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Technician implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "number", nullable = false)
    private String number;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "technician")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<MaintenanceDocument> maintenanceDocuments = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public Technician number(String number) {
        this.number = number;
        return this;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public Technician name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MaintenanceDocument> getMaintenanceDocuments() {
        return maintenanceDocuments;
    }

    public Technician maintenanceDocuments(Set<MaintenanceDocument> maintenanceDocuments) {
        this.maintenanceDocuments = maintenanceDocuments;
        return this;
    }

    public Technician addMaintenanceDocument(MaintenanceDocument maintenanceDocument) {
        this.maintenanceDocuments.add(maintenanceDocument);
        maintenanceDocument.setTechnician(this);
        return this;
    }

    public Technician removeMaintenanceDocument(MaintenanceDocument maintenanceDocument) {
        this.maintenanceDocuments.remove(maintenanceDocument);
        maintenanceDocument.setTechnician(null);
        return this;
    }

    public void setMaintenanceDocuments(Set<MaintenanceDocument> maintenanceDocuments) {
        this.maintenanceDocuments = maintenanceDocuments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Technician)) {
            return false;
        }
        return id != null && id.equals(((Technician) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Technician{" +
            "id=" + getId() +
            ", number='" + getNumber() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
