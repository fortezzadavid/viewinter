package nl.fortezza.interview.domain.enumeration;

/**
 * The DocumentStatus enumeration.
 */
public enum DocumentStatus {
    CREATED, ARCHIVED, DELETED
}
