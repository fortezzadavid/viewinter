package nl.fortezza.interview.domain.enumeration;

/**
 * The Country enumeration.
 */
public enum Country {
    PH, OO, F, D
}
