package nl.fortezza.interview.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nl.fortezza.interview.domain.FortezzaEmployee;
import nl.fortezza.interview.service.FortezzaService;

/**
 * FortezzaResource controller
 */
@RestController
@RequestMapping("/api/fortezza")
public class FortezzaResource {

    private final Logger log = LoggerFactory.getLogger(FortezzaResource.class);
    private final FortezzaService fortezzaService;

    public FortezzaResource(FortezzaService fortezzaService) {
        this.fortezzaService = fortezzaService;
    }

    /**
    * GET getFortezza
    */
    @GetMapping("/fortezza")
    public String getFortezza() {
        return fortezzaService.getEmployees();
//        return "[\n" +
//            "  {\n" +
//            "    \"id\": 1,\n" +
//            "    \"name\": \"jan\",\n" +
//            "    \"age\": 42,\n" +
//            "    \"city\": \"Delft\"\n" +
//            "  },\n" +
//            "  {\n" +
//            "    \"id\": 2,\n" +
//            "    \"name\": \"piet\",\n" +
//            "    \"age\": 22,\n" +
//            "    \"city\": \"Rotterdam\"\n" +
//            "  },\n" +
//            "  {\n" +
//            "    \"id\": 3,\n" +
//            "    \"name\": \"irma\",\n" +
//            "    \"age\": 78,\n" +
//            "    \"city\": \"Zoetermeer\"\n" +
//            "  }\n" +
//            "]";
    }

}
