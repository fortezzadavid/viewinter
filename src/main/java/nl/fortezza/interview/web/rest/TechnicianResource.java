package nl.fortezza.interview.web.rest;

import nl.fortezza.interview.domain.Technician;
import nl.fortezza.interview.service.TechnicianService;
import nl.fortezza.interview.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link nl.fortezza.interview.domain.Technician}.
 */
@RestController
@RequestMapping("/api")
public class TechnicianResource {

    private final Logger log = LoggerFactory.getLogger(TechnicianResource.class);

    private static final String ENTITY_NAME = "technician";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TechnicianService technicianService;

    public TechnicianResource(TechnicianService technicianService) {
        this.technicianService = technicianService;
    }

    /**
     * {@code POST  /technicians} : Create a new technician.
     *
     * @param technician the technician to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new technician, or with status {@code 400 (Bad Request)} if the technician has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/technicians")
    public ResponseEntity<Technician> createTechnician(@Valid @RequestBody Technician technician) throws URISyntaxException {
        log.debug("REST request to save Technician : {}", technician);
        if (technician.getId() != null) {
            throw new BadRequestAlertException("A new technician cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Technician result = technicianService.save(technician);
        return ResponseEntity.created(new URI("/api/technicians/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /technicians} : Updates an existing technician.
     *
     * @param technician the technician to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated technician,
     * or with status {@code 400 (Bad Request)} if the technician is not valid,
     * or with status {@code 500 (Internal Server Error)} if the technician couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/technicians")
    public ResponseEntity<Technician> updateTechnician(@Valid @RequestBody Technician technician) throws URISyntaxException {
        log.debug("REST request to update Technician : {}", technician);
        if (technician.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Technician result = technicianService.save(technician);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, technician.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /technicians} : get all the technicians.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of technicians in body.
     */
    @GetMapping("/technicians")
    public ResponseEntity<List<Technician>> getAllTechnicians(Pageable pageable) {
        log.debug("REST request to get a page of Technicians");
        Page<Technician> page = technicianService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /technicians/:id} : get the "id" technician.
     *
     * @param id the id of the technician to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the technician, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/technicians/{id}")
    public ResponseEntity<Technician> getTechnician(@PathVariable Long id) {
        log.debug("REST request to get Technician : {}", id);
        Optional<Technician> technician = technicianService.findOne(id);
        return ResponseUtil.wrapOrNotFound(technician);
    }

    /**
     * {@code DELETE  /technicians/:id} : delete the "id" technician.
     *
     * @param id the id of the technician to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/technicians/{id}")
    public ResponseEntity<Void> deleteTechnician(@PathVariable Long id) {
        log.debug("REST request to delete Technician : {}", id);
        technicianService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
