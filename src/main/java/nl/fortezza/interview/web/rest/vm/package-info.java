/**
 * View Models used by Spring MVC REST controllers.
 */
package nl.fortezza.interview.web.rest.vm;
