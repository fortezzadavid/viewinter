package nl.fortezza.interview.web.rest;

import nl.fortezza.interview.domain.MaintenanceDocument;
import nl.fortezza.interview.service.MaintenanceDocumentService;
import nl.fortezza.interview.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link nl.fortezza.interview.domain.MaintenanceDocument}.
 */
@RestController
@RequestMapping("/api")
public class MaintenanceDocumentResource {

    private final Logger log = LoggerFactory.getLogger(MaintenanceDocumentResource.class);

    private static final String ENTITY_NAME = "maintenanceDocument";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaintenanceDocumentService maintenanceDocumentService;

    public MaintenanceDocumentResource(MaintenanceDocumentService maintenanceDocumentService) {
        this.maintenanceDocumentService = maintenanceDocumentService;
    }

    /**
     * {@code POST  /maintenance-documents} : Create a new maintenanceDocument.
     *
     * @param maintenanceDocument the maintenanceDocument to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new maintenanceDocument, or with status {@code 400 (Bad Request)} if the maintenanceDocument has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/maintenance-documents")
    public ResponseEntity<MaintenanceDocument> createMaintenanceDocument(@Valid @RequestBody MaintenanceDocument maintenanceDocument) throws URISyntaxException {
        log.debug("REST request to save MaintenanceDocument : {}", maintenanceDocument);
        if (maintenanceDocument.getId() != null) {
            throw new BadRequestAlertException("A new maintenanceDocument cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaintenanceDocument result = maintenanceDocumentService.save(maintenanceDocument);
        return ResponseEntity.created(new URI("/api/maintenance-documents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /maintenance-documents} : Updates an existing maintenanceDocument.
     *
     * @param maintenanceDocument the maintenanceDocument to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated maintenanceDocument,
     * or with status {@code 400 (Bad Request)} if the maintenanceDocument is not valid,
     * or with status {@code 500 (Internal Server Error)} if the maintenanceDocument couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/maintenance-documents")
    public ResponseEntity<MaintenanceDocument> updateMaintenanceDocument(@Valid @RequestBody MaintenanceDocument maintenanceDocument) throws URISyntaxException {
        log.debug("REST request to update MaintenanceDocument : {}", maintenanceDocument);
        if (maintenanceDocument.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MaintenanceDocument result = maintenanceDocumentService.save(maintenanceDocument);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, maintenanceDocument.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /maintenance-documents} : get all the maintenanceDocuments.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of maintenanceDocuments in body.
     */
    @GetMapping("/maintenance-documents")
    public ResponseEntity<List<MaintenanceDocument>> getAllMaintenanceDocuments(Pageable pageable) {
        log.debug("REST request to get a page of MaintenanceDocuments");
        Page<MaintenanceDocument> page = maintenanceDocumentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /maintenance-documents/:id} : get the "id" maintenanceDocument.
     *
     * @param id the id of the maintenanceDocument to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the maintenanceDocument, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/maintenance-documents/{id}")
    public ResponseEntity<MaintenanceDocument> getMaintenanceDocument(@PathVariable Long id) {
        log.debug("REST request to get MaintenanceDocument : {}", id);
        Optional<MaintenanceDocument> maintenanceDocument = maintenanceDocumentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(maintenanceDocument);
    }

    /**
     * {@code DELETE  /maintenance-documents/:id} : delete the "id" maintenanceDocument.
     *
     * @param id the id of the maintenanceDocument to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/maintenance-documents/{id}")
    public ResponseEntity<Void> deleteMaintenanceDocument(@PathVariable Long id) {
        log.debug("REST request to delete MaintenanceDocument : {}", id);
        maintenanceDocumentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
