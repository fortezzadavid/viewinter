import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaintenanceDocument } from 'app/shared/model/maintenance-document.model';

@Component({
  selector: 'jhi-maintenance-document-detail',
  templateUrl: './maintenance-document-detail.component.html',
})
export class MaintenanceDocumentDetailComponent implements OnInit {
  maintenanceDocument: IMaintenanceDocument | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ maintenanceDocument }) => (this.maintenanceDocument = maintenanceDocument));
  }

  previousState(): void {
    window.history.back();
  }
}
