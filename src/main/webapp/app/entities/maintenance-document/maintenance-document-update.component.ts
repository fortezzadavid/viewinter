import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMaintenanceDocument, MaintenanceDocument } from 'app/shared/model/maintenance-document.model';
import { MaintenanceDocumentService } from './maintenance-document.service';
import { IAirplane } from 'app/shared/model/airplane.model';
import { AirplaneService } from 'app/entities/airplane/airplane.service';
import { IDocumentType } from 'app/shared/model/document-type.model';
import { DocumentTypeService } from 'app/entities/document-type/document-type.service';
import { ITechnician } from 'app/shared/model/technician.model';
import { TechnicianService } from 'app/entities/technician/technician.service';

type SelectableEntity = IAirplane | IDocumentType | ITechnician;

@Component({
  selector: 'jhi-maintenance-document-update',
  templateUrl: './maintenance-document-update.component.html',
})
export class MaintenanceDocumentUpdateComponent implements OnInit {
  isSaving = false;
  airplanes: IAirplane[] = [];
  documenttypes: IDocumentType[] = [];
  technicians: ITechnician[] = [];

  editForm = this.fb.group({
    id: [],
    filename: [null, [Validators.required]],
    version: [null, [Validators.required]],
    status: [null, [Validators.required]],
    airplane: [],
    type: [],
    technician: [],
  });

  constructor(
    protected maintenanceDocumentService: MaintenanceDocumentService,
    protected airplaneService: AirplaneService,
    protected documentTypeService: DocumentTypeService,
    protected technicianService: TechnicianService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ maintenanceDocument }) => {
      this.updateForm(maintenanceDocument);

      this.airplaneService.query().subscribe((res: HttpResponse<IAirplane[]>) => (this.airplanes = res.body || []));

      this.documentTypeService.query().subscribe((res: HttpResponse<IDocumentType[]>) => (this.documenttypes = res.body || []));

      this.technicianService.query().subscribe((res: HttpResponse<ITechnician[]>) => (this.technicians = res.body || []));
    });
  }

  updateForm(maintenanceDocument: IMaintenanceDocument): void {
    this.editForm.patchValue({
      id: maintenanceDocument.id,
      filename: maintenanceDocument.filename,
      version: maintenanceDocument.version,
      status: maintenanceDocument.status,
      airplane: maintenanceDocument.airplane,
      type: maintenanceDocument.type,
      technician: maintenanceDocument.technician,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const maintenanceDocument = this.createFromForm();
    if (maintenanceDocument.id !== undefined) {
      this.subscribeToSaveResponse(this.maintenanceDocumentService.update(maintenanceDocument));
    } else {
      this.subscribeToSaveResponse(this.maintenanceDocumentService.create(maintenanceDocument));
    }
  }

  private createFromForm(): IMaintenanceDocument {
    return {
      ...new MaintenanceDocument(),
      id: this.editForm.get(['id'])!.value,
      filename: this.editForm.get(['filename'])!.value,
      version: this.editForm.get(['version'])!.value,
      status: this.editForm.get(['status'])!.value,
      airplane: this.editForm.get(['airplane'])!.value,
      type: this.editForm.get(['type'])!.value,
      technician: this.editForm.get(['technician'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaintenanceDocument>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
