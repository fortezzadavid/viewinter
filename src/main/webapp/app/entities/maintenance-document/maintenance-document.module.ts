import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FortezzaInterviewSharedModule } from 'app/shared/shared.module';
import { MaintenanceDocumentComponent } from './maintenance-document.component';
import { MaintenanceDocumentDetailComponent } from './maintenance-document-detail.component';
import { MaintenanceDocumentUpdateComponent } from './maintenance-document-update.component';
import { MaintenanceDocumentDeleteDialogComponent } from './maintenance-document-delete-dialog.component';
import { maintenanceDocumentRoute } from './maintenance-document.route';

@NgModule({
  imports: [FortezzaInterviewSharedModule, RouterModule.forChild(maintenanceDocumentRoute)],
  declarations: [
    MaintenanceDocumentComponent,
    MaintenanceDocumentDetailComponent,
    MaintenanceDocumentUpdateComponent,
    MaintenanceDocumentDeleteDialogComponent,
  ],
  entryComponents: [MaintenanceDocumentDeleteDialogComponent],
})
export class FortezzaInterviewMaintenanceDocumentModule {}
