import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IMaintenanceDocument } from 'app/shared/model/maintenance-document.model';

type EntityResponseType = HttpResponse<IMaintenanceDocument>;
type EntityArrayResponseType = HttpResponse<IMaintenanceDocument[]>;

@Injectable({ providedIn: 'root' })
export class MaintenanceDocumentService {
  public resourceUrl = SERVER_API_URL + 'api/maintenance-documents';

  constructor(protected http: HttpClient) {}

  create(maintenanceDocument: IMaintenanceDocument): Observable<EntityResponseType> {
    return this.http.post<IMaintenanceDocument>(this.resourceUrl, maintenanceDocument, { observe: 'response' });
  }

  update(maintenanceDocument: IMaintenanceDocument): Observable<EntityResponseType> {
    return this.http.put<IMaintenanceDocument>(this.resourceUrl, maintenanceDocument, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMaintenanceDocument>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMaintenanceDocument[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
