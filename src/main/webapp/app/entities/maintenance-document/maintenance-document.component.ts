import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMaintenanceDocument } from 'app/shared/model/maintenance-document.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { MaintenanceDocumentService } from './maintenance-document.service';
import { MaintenanceDocumentDeleteDialogComponent } from './maintenance-document-delete-dialog.component';

@Component({
  selector: 'jhi-maintenance-document',
  templateUrl: './maintenance-document.component.html',
})
export class MaintenanceDocumentComponent implements OnInit, OnDestroy {
  maintenanceDocuments: IMaintenanceDocument[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected maintenanceDocumentService: MaintenanceDocumentService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.maintenanceDocuments = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.maintenanceDocumentService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IMaintenanceDocument[]>) => this.paginateMaintenanceDocuments(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.maintenanceDocuments = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInMaintenanceDocuments();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IMaintenanceDocument): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInMaintenanceDocuments(): void {
    this.eventSubscriber = this.eventManager.subscribe('maintenanceDocumentListModification', () => this.reset());
  }

  delete(maintenanceDocument: IMaintenanceDocument): void {
    const modalRef = this.modalService.open(MaintenanceDocumentDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.maintenanceDocument = maintenanceDocument;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateMaintenanceDocuments(data: IMaintenanceDocument[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.maintenanceDocuments.push(data[i]);
      }
    }
  }
}
