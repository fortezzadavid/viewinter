import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMaintenanceDocument } from 'app/shared/model/maintenance-document.model';
import { MaintenanceDocumentService } from './maintenance-document.service';

@Component({
  templateUrl: './maintenance-document-delete-dialog.component.html',
})
export class MaintenanceDocumentDeleteDialogComponent {
  maintenanceDocument?: IMaintenanceDocument;

  constructor(
    protected maintenanceDocumentService: MaintenanceDocumentService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.maintenanceDocumentService.delete(id).subscribe(() => {
      this.eventManager.broadcast('maintenanceDocumentListModification');
      this.activeModal.close();
    });
  }
}
