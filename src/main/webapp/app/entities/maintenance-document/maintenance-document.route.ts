import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IMaintenanceDocument, MaintenanceDocument } from 'app/shared/model/maintenance-document.model';
import { MaintenanceDocumentService } from './maintenance-document.service';
import { MaintenanceDocumentComponent } from './maintenance-document.component';
import { MaintenanceDocumentDetailComponent } from './maintenance-document-detail.component';
import { MaintenanceDocumentUpdateComponent } from './maintenance-document-update.component';

@Injectable({ providedIn: 'root' })
export class MaintenanceDocumentResolve implements Resolve<IMaintenanceDocument> {
  constructor(private service: MaintenanceDocumentService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMaintenanceDocument> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((maintenanceDocument: HttpResponse<MaintenanceDocument>) => {
          if (maintenanceDocument.body) {
            return of(maintenanceDocument.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MaintenanceDocument());
  }
}

export const maintenanceDocumentRoute: Routes = [
  {
    path: '',
    component: MaintenanceDocumentComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.maintenanceDocument.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MaintenanceDocumentDetailComponent,
    resolve: {
      maintenanceDocument: MaintenanceDocumentResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.maintenanceDocument.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MaintenanceDocumentUpdateComponent,
    resolve: {
      maintenanceDocument: MaintenanceDocumentResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.maintenanceDocument.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MaintenanceDocumentUpdateComponent,
    resolve: {
      maintenanceDocument: MaintenanceDocumentResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.maintenanceDocument.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
