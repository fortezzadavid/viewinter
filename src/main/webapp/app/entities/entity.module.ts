import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'technician',
        loadChildren: () => import('./technician/technician.module').then(m => m.FortezzaInterviewTechnicianModule),
      },
      {
        path: 'airplane',
        loadChildren: () => import('./airplane/airplane.module').then(m => m.FortezzaInterviewAirplaneModule),
      },
      {
        path: 'maintenance-document',
        loadChildren: () =>
          import('./maintenance-document/maintenance-document.module').then(m => m.FortezzaInterviewMaintenanceDocumentModule),
      },
      {
        path: 'document-type',
        loadChildren: () => import('./document-type/document-type.module').then(m => m.FortezzaInterviewDocumentTypeModule),
      },
      {
        path: 'fortezza-employees',
        loadChildren: () => import('./fortezza-employees/fortezza-employees.module').then(m => m.FortezzaInterviewFortezzaEmployeesModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class FortezzaInterviewEntityModule {}
