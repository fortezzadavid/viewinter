import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FortezzaInterviewSharedModule } from 'app/shared/shared.module';
import { FortezzaEmployeesComponent } from './fortezza-employees.component';
import { FortezzaEmployeesDetailComponent } from './fortezza-employees-detail.component';
import { FortezzaEmployeesUpdateComponent } from './fortezza-employees-update.component';
import { FortezzaEmployeesDeleteDialogComponent } from './fortezza-employees-delete-dialog.component';
import { fortezzaEmployeesRoute } from './fortezza-employees.route';

@NgModule({
  imports: [FortezzaInterviewSharedModule, RouterModule.forChild(fortezzaEmployeesRoute)],
  declarations: [
    FortezzaEmployeesComponent,
    FortezzaEmployeesDetailComponent,
    FortezzaEmployeesUpdateComponent,
    FortezzaEmployeesDeleteDialogComponent,
  ],
  entryComponents: [FortezzaEmployeesDeleteDialogComponent],
})
export class FortezzaInterviewFortezzaEmployeesModule {}
