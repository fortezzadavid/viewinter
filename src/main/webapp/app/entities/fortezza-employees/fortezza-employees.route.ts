import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFortezzaEmployees, FortezzaEmployees } from 'app/shared/model/fortezza-employees.model';
import { FortezzaEmployeesService } from './fortezza-employees.service';
import { FortezzaEmployeesComponent } from './fortezza-employees.component';
import { FortezzaEmployeesDetailComponent } from './fortezza-employees-detail.component';
import { FortezzaEmployeesUpdateComponent } from './fortezza-employees-update.component';

@Injectable({ providedIn: 'root' })
export class FortezzaEmployeesResolve implements Resolve<IFortezzaEmployees> {
  constructor(private service: FortezzaEmployeesService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFortezzaEmployees> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fortezzaEmployees: HttpResponse<FortezzaEmployees>) => {
          if (fortezzaEmployees.body) {
            return of(fortezzaEmployees.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FortezzaEmployees());
  }
}

export const fortezzaEmployeesRoute: Routes = [
  {
    path: '',
    component: FortezzaEmployeesComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.fortezzaEmployees.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FortezzaEmployeesDetailComponent,
    resolve: {
      fortezzaEmployees: FortezzaEmployeesResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.fortezzaEmployees.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FortezzaEmployeesUpdateComponent,
    resolve: {
      fortezzaEmployees: FortezzaEmployeesResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.fortezzaEmployees.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FortezzaEmployeesUpdateComponent,
    resolve: {
      fortezzaEmployees: FortezzaEmployeesResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.fortezzaEmployees.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
