import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFortezzaEmployees } from 'app/shared/model/fortezza-employees.model';

type EntityResponseType = HttpResponse<IFortezzaEmployees>;
type EntityArrayResponseType = HttpResponse<IFortezzaEmployees[]>;

@Injectable({ providedIn: 'root' })
export class FortezzaEmployeesService {
  public resourceUrl = SERVER_API_URL + 'api/fortezza/fortezza';

  constructor(protected http: HttpClient) {}

  create(fortezzaEmployees: IFortezzaEmployees): Observable<EntityResponseType> {
    return this.http.post<IFortezzaEmployees>(this.resourceUrl, fortezzaEmployees, { observe: 'response' });
  }

  update(fortezzaEmployees: IFortezzaEmployees): Observable<EntityResponseType> {
    return this.http.put<IFortezzaEmployees>(this.resourceUrl, fortezzaEmployees, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFortezzaEmployees>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFortezzaEmployees[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
