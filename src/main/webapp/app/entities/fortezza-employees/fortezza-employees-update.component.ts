import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFortezzaEmployees, FortezzaEmployees } from 'app/shared/model/fortezza-employees.model';
import { FortezzaEmployeesService } from './fortezza-employees.service';

@Component({
  selector: 'jhi-fortezza-employees-update',
  templateUrl: './fortezza-employees-update.component.html',
})
export class FortezzaEmployeesUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    age: [],
    city: [],
  });

  constructor(
    protected fortezzaEmployeesService: FortezzaEmployeesService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fortezzaEmployees }) => {
      this.updateForm(fortezzaEmployees);
    });
  }

  updateForm(fortezzaEmployees: IFortezzaEmployees): void {
    this.editForm.patchValue({
      id: fortezzaEmployees.id,
      name: fortezzaEmployees.name,
      age: fortezzaEmployees.age,
      city: fortezzaEmployees.city,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fortezzaEmployees = this.createFromForm();
    if (fortezzaEmployees.id !== undefined) {
      this.subscribeToSaveResponse(this.fortezzaEmployeesService.update(fortezzaEmployees));
    } else {
      this.subscribeToSaveResponse(this.fortezzaEmployeesService.create(fortezzaEmployees));
    }
  }

  private createFromForm(): IFortezzaEmployees {
    return {
      ...new FortezzaEmployees(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      age: this.editForm.get(['age'])!.value,
      city: this.editForm.get(['city'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFortezzaEmployees>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
