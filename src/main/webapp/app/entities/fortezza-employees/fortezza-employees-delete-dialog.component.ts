import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFortezzaEmployees } from 'app/shared/model/fortezza-employees.model';
import { FortezzaEmployeesService } from './fortezza-employees.service';

@Component({
  templateUrl: './fortezza-employees-delete-dialog.component.html',
})
export class FortezzaEmployeesDeleteDialogComponent {
  fortezzaEmployees?: IFortezzaEmployees;

  constructor(
    protected fortezzaEmployeesService: FortezzaEmployeesService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fortezzaEmployeesService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fortezzaEmployeesListModification');
      this.activeModal.close();
    });
  }
}
