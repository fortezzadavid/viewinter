import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFortezzaEmployees } from 'app/shared/model/fortezza-employees.model';
import { FortezzaEmployeesService } from './fortezza-employees.service';
import { FortezzaEmployeesDeleteDialogComponent } from './fortezza-employees-delete-dialog.component';

@Component({
  selector: 'jhi-fortezza-employees',
  templateUrl: './fortezza-employees.component.html',
})
export class FortezzaEmployeesComponent implements OnInit, OnDestroy {
  fortezzaEmployees?: IFortezzaEmployees[];
  eventSubscriber?: Subscription;

  constructor(
    protected fortezzaEmployeesService: FortezzaEmployeesService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.fortezzaEmployeesService.query().subscribe((res: HttpResponse<IFortezzaEmployees[]>) => (this.fortezzaEmployees = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFortezzaEmployees();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFortezzaEmployees): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFortezzaEmployees(): void {
    this.eventSubscriber = this.eventManager.subscribe('fortezzaEmployeesListModification', () => this.loadAll());
  }

  delete(fortezzaEmployees: IFortezzaEmployees): void {
    const modalRef = this.modalService.open(FortezzaEmployeesDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fortezzaEmployees = fortezzaEmployees;
  }
}
