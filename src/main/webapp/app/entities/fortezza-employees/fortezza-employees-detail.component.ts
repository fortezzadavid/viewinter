import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFortezzaEmployees } from 'app/shared/model/fortezza-employees.model';

@Component({
  selector: 'jhi-fortezza-employees-detail',
  templateUrl: './fortezza-employees-detail.component.html',
})
export class FortezzaEmployeesDetailComponent implements OnInit {
  fortezzaEmployees: IFortezzaEmployees | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fortezzaEmployees }) => (this.fortezzaEmployees = fortezzaEmployees));
  }

  previousState(): void {
    window.history.back();
  }
}
