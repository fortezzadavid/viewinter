import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAirplane } from 'app/shared/model/airplane.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { AirplaneService } from './airplane.service';
import { AirplaneDeleteDialogComponent } from './airplane-delete-dialog.component';

@Component({
  selector: 'jhi-airplane',
  templateUrl: './airplane.component.html',
})
export class AirplaneComponent implements OnInit, OnDestroy {
  airplanes: IAirplane[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected airplaneService: AirplaneService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.airplanes = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.airplaneService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IAirplane[]>) => this.paginateAirplanes(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.airplanes = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAirplanes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAirplane): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAirplanes(): void {
    this.eventSubscriber = this.eventManager.subscribe('airplaneListModification', () => this.reset());
  }

  delete(airplane: IAirplane): void {
    const modalRef = this.modalService.open(AirplaneDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.airplane = airplane;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateAirplanes(data: IAirplane[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.airplanes.push(data[i]);
      }
    }
  }
}
