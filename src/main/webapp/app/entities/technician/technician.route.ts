import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITechnician, Technician } from 'app/shared/model/technician.model';
import { TechnicianService } from './technician.service';
import { TechnicianComponent } from './technician.component';
import { TechnicianDetailComponent } from './technician-detail.component';
import { TechnicianUpdateComponent } from './technician-update.component';

@Injectable({ providedIn: 'root' })
export class TechnicianResolve implements Resolve<ITechnician> {
  constructor(private service: TechnicianService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITechnician> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((technician: HttpResponse<Technician>) => {
          if (technician.body) {
            return of(technician.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Technician());
  }
}

export const technicianRoute: Routes = [
  {
    path: '',
    component: TechnicianComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.technician.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TechnicianDetailComponent,
    resolve: {
      technician: TechnicianResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.technician.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TechnicianUpdateComponent,
    resolve: {
      technician: TechnicianResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.technician.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TechnicianUpdateComponent,
    resolve: {
      technician: TechnicianResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'fortezzaInterviewApp.technician.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
