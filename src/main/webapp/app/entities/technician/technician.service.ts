import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITechnician } from 'app/shared/model/technician.model';

type EntityResponseType = HttpResponse<ITechnician>;
type EntityArrayResponseType = HttpResponse<ITechnician[]>;

@Injectable({ providedIn: 'root' })
export class TechnicianService {
  public resourceUrl = SERVER_API_URL + 'api/technicians';

  constructor(protected http: HttpClient) {}

  create(technician: ITechnician): Observable<EntityResponseType> {
    return this.http.post<ITechnician>(this.resourceUrl, technician, { observe: 'response' });
  }

  update(technician: ITechnician): Observable<EntityResponseType> {
    return this.http.put<ITechnician>(this.resourceUrl, technician, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITechnician>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITechnician[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
