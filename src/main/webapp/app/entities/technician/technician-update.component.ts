import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITechnician, Technician } from 'app/shared/model/technician.model';
import { TechnicianService } from './technician.service';

@Component({
  selector: 'jhi-technician-update',
  templateUrl: './technician-update.component.html',
})
export class TechnicianUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    number: [null, [Validators.required]],
    name: [null, [Validators.required]],
  });

  constructor(protected technicianService: TechnicianService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ technician }) => {
      this.updateForm(technician);
    });
  }

  updateForm(technician: ITechnician): void {
    this.editForm.patchValue({
      id: technician.id,
      number: technician.number,
      name: technician.name,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const technician = this.createFromForm();
    if (technician.id !== undefined) {
      this.subscribeToSaveResponse(this.technicianService.update(technician));
    } else {
      this.subscribeToSaveResponse(this.technicianService.create(technician));
    }
  }

  private createFromForm(): ITechnician {
    return {
      ...new Technician(),
      id: this.editForm.get(['id'])!.value,
      number: this.editForm.get(['number'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITechnician>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
