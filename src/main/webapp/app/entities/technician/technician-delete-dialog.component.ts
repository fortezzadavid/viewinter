import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITechnician } from 'app/shared/model/technician.model';
import { TechnicianService } from './technician.service';

@Component({
  templateUrl: './technician-delete-dialog.component.html',
})
export class TechnicianDeleteDialogComponent {
  technician?: ITechnician;

  constructor(
    protected technicianService: TechnicianService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.technicianService.delete(id).subscribe(() => {
      this.eventManager.broadcast('technicianListModification');
      this.activeModal.close();
    });
  }
}
