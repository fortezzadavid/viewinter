import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITechnician } from 'app/shared/model/technician.model';

@Component({
  selector: 'jhi-technician-detail',
  templateUrl: './technician-detail.component.html',
})
export class TechnicianDetailComponent implements OnInit {
  technician: ITechnician | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ technician }) => (this.technician = technician));
  }

  previousState(): void {
    window.history.back();
  }
}
