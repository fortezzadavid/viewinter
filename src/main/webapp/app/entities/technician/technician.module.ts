import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FortezzaInterviewSharedModule } from 'app/shared/shared.module';
import { TechnicianComponent } from './technician.component';
import { TechnicianDetailComponent } from './technician-detail.component';
import { TechnicianUpdateComponent } from './technician-update.component';
import { TechnicianDeleteDialogComponent } from './technician-delete-dialog.component';
import { technicianRoute } from './technician.route';

@NgModule({
  imports: [FortezzaInterviewSharedModule, RouterModule.forChild(technicianRoute)],
  declarations: [TechnicianComponent, TechnicianDetailComponent, TechnicianUpdateComponent, TechnicianDeleteDialogComponent],
  entryComponents: [TechnicianDeleteDialogComponent],
})
export class FortezzaInterviewTechnicianModule {}
