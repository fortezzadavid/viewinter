import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITechnician } from 'app/shared/model/technician.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TechnicianService } from './technician.service';
import { TechnicianDeleteDialogComponent } from './technician-delete-dialog.component';

@Component({
  selector: 'jhi-technician',
  templateUrl: './technician.component.html',
})
export class TechnicianComponent implements OnInit, OnDestroy {
  technicians: ITechnician[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected technicianService: TechnicianService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.technicians = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.technicianService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<ITechnician[]>) => this.paginateTechnicians(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.technicians = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTechnicians();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITechnician): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTechnicians(): void {
    this.eventSubscriber = this.eventManager.subscribe('technicianListModification', () => this.reset());
  }

  delete(technician: ITechnician): void {
    const modalRef = this.modalService.open(TechnicianDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.technician = technician;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateTechnicians(data: ITechnician[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.technicians.push(data[i]);
      }
    }
  }
}
