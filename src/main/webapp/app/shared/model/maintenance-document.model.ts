import { IAirplane } from 'app/shared/model/airplane.model';
import { IDocumentType } from 'app/shared/model/document-type.model';
import { ITechnician } from 'app/shared/model/technician.model';
import { DocumentStatus } from 'app/shared/model/enumerations/document-status.model';

export interface IMaintenanceDocument {
  id?: number;
  filename?: string;
  version?: string;
  status?: DocumentStatus;
  airplane?: IAirplane;
  type?: IDocumentType;
  technician?: ITechnician;
}

export class MaintenanceDocument implements IMaintenanceDocument {
  constructor(
    public id?: number,
    public filename?: string,
    public version?: string,
    public status?: DocumentStatus,
    public airplane?: IAirplane,
    public type?: IDocumentType,
    public technician?: ITechnician
  ) {}
}
