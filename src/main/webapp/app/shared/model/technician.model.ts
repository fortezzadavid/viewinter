import { IMaintenanceDocument } from 'app/shared/model/maintenance-document.model';

export interface ITechnician {
  id?: number;
  number?: string;
  name?: string;
  maintenanceDocuments?: IMaintenanceDocument[];
}

export class Technician implements ITechnician {
  constructor(public id?: number, public number?: string, public name?: string, public maintenanceDocuments?: IMaintenanceDocument[]) {}
}
