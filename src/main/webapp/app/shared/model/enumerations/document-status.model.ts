export const enum DocumentStatus {
  CREATED = 'CREATED',

  ARCHIVED = 'ARCHIVED',

  DELETED = 'DELETED',
}
