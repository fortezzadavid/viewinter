import { IMaintenanceDocument } from 'app/shared/model/maintenance-document.model';
import { Country } from 'app/shared/model/enumerations/country.model';

export interface IAirplane {
  id?: number;
  registration?: string;
  clubOwned?: boolean;
  country?: Country;
  maintenanceDocuments?: IMaintenanceDocument[];
}

export class Airplane implements IAirplane {
  constructor(
    public id?: number,
    public registration?: string,
    public clubOwned?: boolean,
    public country?: Country,
    public maintenanceDocuments?: IMaintenanceDocument[]
  ) {
    this.clubOwned = this.clubOwned || false;
  }
}
