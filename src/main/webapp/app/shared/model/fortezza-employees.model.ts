export interface IFortezzaEmployees {
  id?: number;
  name?: string;
  age?: number;
  city?: string;
}

export class FortezzaEmployees implements IFortezzaEmployees {
  constructor(public id?: number, public name?: string, public age?: number, public city?: string) {}
}
