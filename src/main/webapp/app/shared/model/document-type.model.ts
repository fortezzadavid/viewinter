import { IMaintenanceDocument } from 'app/shared/model/maintenance-document.model';

export interface IDocumentType {
  id?: number;
  name?: string;
  desciption?: string;
  maintenanceDocuments?: IMaintenanceDocument[];
}

export class DocumentType implements IDocumentType {
  constructor(public id?: number, public name?: string, public desciption?: string, public maintenanceDocuments?: IMaintenanceDocument[]) {}
}
