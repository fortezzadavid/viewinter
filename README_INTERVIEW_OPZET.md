# Interview
Beste kandidaat,

Onderstaande test neemt maximaal 45 minuten in beslag. 

Voordat je de test krijgt, controleer of het volgende is geïnstalleerd:
- Java 8 of hoger
- Maven
- Node.js / npm
- geadviseerd Eclipse / Intellij
- Gitlab account (voor het clonen van de opdracht en het maken van een pull request)

Na het ontvangen van de Gitlab URL maak je in Gitlab een clone van het project aan. 
Je krijgt je 45 minuten voor het beantwoorden van 6 vragen. Na het maken van de 
test, commit je de resultaten in Git, push je ze naar Gitlab en maak je een 
pull request. Daarmee gaan we de antwoorden reviewen. 

Een aantal punten vooraf:
- Je mag gebruik maken van het internet voor informatie
- Als je vragen hebt of niet gelijk weet wat je moet doen, stel deze direct want je hebt niet veel tijd.
- Na 45 minuten mag je niet meer verder werken.
- Het is niet erg als je niet alles afkrijgt. 
- Vooraf nog vragen over de opzet? Is deze duidelijk?

N.B. Deze test is vertrouwelijk. Informatie hierover mag niet gedeeld worden met derden (zoals o.a. 
recruitment medewerkers)
